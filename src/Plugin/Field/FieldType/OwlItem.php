<?php

namespace Drupal\owl\Plugin\Field\FieldType;

use Drupal\Core\Field\FieldStorageDefinitionInterface;
use Drupal\Core\TypedData\DataDefinition;
use Drupal\image\Plugin\Field\FieldType\ImageItem;


/**
 * Plugin implementation of the 'image' field type.
 *
 * @FieldType(
 *   id = "owl_image",
 *   label = @Translation("Owl Image"),
 *   description = @Translation("This field stores the ID of an image file as an integer value."),
 *   category = @Translation("Reference"),
 *   default_widget = "owl_image_image",
 *   default_formatter = "owl_field_formatter",
 *   column_groups = {
 *     "file" = {
 *       "label" = @Translation("File"),
 *       "columns" = {
 *         "target_id", "width", "height"
 *       },
 *       "require_all_groups_for_translation" = TRUE
 *     },
 *     "alt" = {
 *       "label" = @Translation("Alt"),
 *       "translatable" = TRUE
 *     },
 *     "title" = {
 *       "label" = @Translation("Title"),
 *       "translatable" = TRUE
 *     },
 *     "caption" = {
 *       "label" = @Translation("Caption"),
 *       "translatable" = TRUE
 *     },
 *   },
 *   list_class = "\Drupal\file\Plugin\Field\FieldType\FileFieldItemList",
 * )
 */


class OwlItem extends ImageItem {

    public static function schema(FieldStorageDefinitionInterface $field_definition) {
        $schema = parent::schema($field_definition);
        $schema['columns']['owl_title'] = array(
             'type' => 'varchar',
             'length' => '1024',
             'description' => "Image caption text",
         );

        $schema['columns']['owl_paragraph'] = array(
            'type' => 'varchar',
            'length' => '1024',
            'description' => "Image caption text",
        );


        return $schema;

    }


    public static function propertyDefinitions(FieldStorageDefinitionInterface $field_definition) {
        $properties = parent::propertyDefinitions($field_definition);

        $properties['owl_title'] = DataDefinition::create('string')
            ->setLabel(t('Owl title'))
            ->setDescription(t("Slide title text."));


        $properties['owl_paragraph'] = DataDefinition::create('string')
            ->setLabel(t('Owl paragraph'))
            ->setDescription(t("Slide paragraph text"));

        return $properties;
    }

}