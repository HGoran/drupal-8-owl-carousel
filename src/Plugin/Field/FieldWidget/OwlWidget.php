<?php


namespace Drupal\owl\Plugin\Field\FieldWidget;

use Drupal\Core\Field\FieldItemListInterface;
use Drupal\Component\Utility\NestedArray;
use Drupal\Core\Form\FormStateInterface;
use Drupal\file\Entity\File;
use Drupal\file\Plugin\Field\FieldWidget\FileWidget;
use Drupal\image\Entity\ImageStyle;
use Drupal\image\Plugin\Field\FieldWidget\ImageWidget;


/**
 * Plugin implementation of the 'owl_image' widget.
 *
 * @FieldWidget(
 *   id = "owl_image_image",
 *   module = "owl",
 *   label = @Translation("Owl Image"),
 *   field_types = {
 *     "owl_image"
 *   }
 * )
 */

class OwlWidget extends ImageWidget {

    /**
     * Form API callback: Processes a image_image field element.
     *
     * Expands the image_image type to include the alt and title fields.
     *
     * This method is assigned as a #process callback in formElement() method.
     */
    public static function process($element, FormStateInterface $form_state, $form) {
        $item = $element['#value'];
        $item['fids'] = $element['fids']['#value'];

        $element['owl_title'] = array(
            '#type' => 'textfield',
            '#title' => t('Slide title'),
            '#default_value' => isset($item['owl_title']) ? $item['owl_title'] : '',
            '#description' => t('Slide title text '),
            '#maxlength' => 1024,
            '#weight' => -11,
        );

        $element['owl_paragraph'] = array(
            '#type' => 'textarea',
            '#title' => t('Slide caption'),
            '#default_value' => isset($item['owl_paragraph']) ? $item['owl_paragraph'] : '',
            '#description' => t('Slide paragraph text'),
            '#maxlength' => 1024,
            '#weight' => -11,
        );

        return parent::process($element, $form_state, $form);
    }

}
