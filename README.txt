--- README  -------------------------------------------------------------

Owl carousel
Licensed under GNU GENERAL PUBLIC LICENSE.

Provides  owl carousel field types and display settings to create owl carousel

Disclaimer: This module was made for In-house purposes of Lab Lateral. http://lab.co.uk.

--- INSTALLATION --------------------------------------------------------

1 - Clone the module into /modules directory with the following : git clone git@bitbucket.org:HGoran/drupal-8-owl-carousel.git

--- USAGE ---------------------------------------------------------------

Tutorial video - https://goo.gl/QktR5G

1 - Enable the module at /admin/modules.
2 - Create or edit a content type at /admin/structure/types and
    include an Image field.
3 - Edit this image field, so that multiple image files may be added
    ("Number of values" setting at admin/structure/types/manage/
    {content type}/fields/{field_image}).
4 - Go to "Manage display" for your content type
    (/admin/structure/types/manage/{content type}/display) and
    switch the format of your multiple image field from Image to Owl Carousel if you wish to have a slider with the captions on the images. Otherwise set the image field to No caption image.
5 - Click the settings icon and add your settings.
6 - Save! and here you go.

--- Owl with views -----------------------------------------

Tutorial video - https://goo.gl/7tMq1A

1 - Create a view block
2 - Select the display format to be Owl Carousel of fields
3 - Click on Save and edit
4 -Edit format settings which will have owl settings
5 - Update and save the settings.
6 - Add image


--- AVAILABLE OPTIONS ---------------------------------------------------

Image styles
Link image
Items
Items Desktop
Items Desktop Small
Items Tablet
Items Mobile
Single item
Items ScaleUp
Slide Speed
Pagination Speed
Rewind Speed
Autoplay
Stop on Hover
Navigation
Prev Text
Next Text
Rewind Nav
Scroll per Page
Pagination
Pagination numbers
Responsive
Responsive refresh rate
Mouse Drag
Touch Drag
Image pagination
Transition style


Written by Goran Horvat/Lead frontend developer of Croatian office at Lab Lateral
LinkedIn: https://www.linkedin.com/in/ghorvat/

