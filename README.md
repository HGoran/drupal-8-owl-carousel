#OWL Carousel for Drupal 8

##Website: https://lab.co.uk

###License: GNU GENERAL PUBLIC LICENSE
###Provides  owl carousel field types and display settings to create owl carousel
###Disclaimer: This module was made for In-house purposes of Lab Lateral. http://lab.co.uk.
