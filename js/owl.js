(function ($) {
    Drupal.behaviors.owl = {
        attach: function (context, settings) {
            var $this = null;
            $('.owl-slider-wrapper', context).each(function () {
                $this = $(this);
                var $this_settings = $.parseJSON($this.attr('data-settings'));
                if ($this_settings.imagePagination) {
                    $this_settings.afterInit = makePagination;
                    $this_settings.afterUpdate = makePagination;
                    $this_settings.afterMove = changeSlides;

                }
                $this.owlCarousel($this_settings);
            });

            function makePagination() {
                var $pagination = this.owlControls.find(".owl-page");
                $.each(this.owl.userItems, function (i) {
                        $($pagination).eq(i)
                        .css({
                            'background': 'url(' + $(this).find('img').attr('src') + ')',
                            'background-size': 'cover',
                            'background-repeat': 'no-repeat'
                        })
                });
            }
            function changeSlides() {
                var $pagination = this.owlControls.find(".owl-page"),
                    curSlide = 0;
                $($pagination).each(function () {
                    var self = $(this);
                    if ($(self).hasClass("active")) {
                        if ($(self).prevAll().length <= 1) {
                            curSlide = 0;
                        } else if ($(self).nextAll().length >= 1) {
                            curSlide = curSlide - $(self).prevAll().length + 2;

                        } else if ($(self).nextAll().length == 0) {
                            curSlide = curSlide - $(self).prevAll().length + $(self).prevAll().length - 1;
                        }
                    }
                    $pagination.css({
                        "transform": "translate3d(" + curSlide * ($(self).outerWidth() + 20) + "px,0,0)",
                        "transition": "all 300ms ease-in-out"
                    });
                });
            }
        }
    };
})(jQuery);